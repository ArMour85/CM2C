# Small Python script to convert any video files with subtitle(s) to video supported by Google Chromecast.
#
# Copyright 2021 Arnaud Moura <arnaudmoura@gmail.com>
# This code is released under the terms of the MIT license. See the LICENSE
# file for more details.

# !/usr/bin/env python
# -*- coding: utf-8 -*-


# System
import re
import subprocess
import sys
if sys.version_info >= (3, 0):
    import queue
else:
    import Queue as queue
import threading
import time
import datetime


# function to convert ffmpeg time in second
def ffmpegTimeToSecond(ffmpe_time):
    duration_time = time.strptime(ffmpe_time.split('.')[0],'%H:%M:%S')
    return datetime.timedelta(hours=duration_time.tm_hour,minutes=duration_time.tm_min,seconds=duration_time.tm_sec).total_seconds()

class Convertor:
    # function to run external command
    @staticmethod
    def runCommand(cmd):
        no_error = True

        try:
            subprocess.check_output(cmd, stderr=subprocess.PIPE, shell=True)
        except subprocess.CalledProcessError as e:
            if sys.version_info >= (3, 0):
                print("Conversion error : " + e.stderr.decode('utf-8'))
            else:
                print("Conversion error : " + e.output.decode('utf-8'))
            no_error = False

        return no_error


    # function to run external command in thread with progress bar
    @staticmethod
    def runCommandInThread(cmd, p_progress_bar):
        result_queue = queue.Queue()
        convert_thread = threading.Thread(target=lambda q, arg: q.put(Convertor.runCommand(arg)), args=(result_queue, cmd), daemon=True)
        convert_thread.start()
        while convert_thread.is_alive():
            time.sleep(1)
            p_progress_bar.update()

        print('\n')
        return result_queue.get()


    # function to run video conversion
    # @param p_retutn_queue: (error : bool, time)
    @staticmethod
    def runVideoConversionCommand(cmd, p_return_queue):
        re_time = re.compile(r'time=\d\d:\d\d:\d\d.\d\d', re.U)
        re_duration = re.compile(r'Duration: \d\d:\d\d:\d\d.\d\d', re.U)
        total_duration = 0

        pipe = subprocess.Popen(cmd, shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines = True)

        no_error = False
        prev_line = ""
        frame_line = ""
        while True:
            prev_line = frame_line
            frame_line = pipe.stdout.readline().strip()

            # test if last line
            if frame_line == '' and pipe.poll() is not None:
                if not no_error:
                    p_return_queue.put(RuntimeError("FFMPEG error : ", prev_line))
                break

            # get frame number of progression
            time_match = re_time.search(frame_line)
            if time_match:
                no_error = True
                
                # if the progress bar with frame indicator exists
                if total_duration != 0:
                    time_value = time_match.group(0).split('=')[1]
                    time_value_second = ffmpegTimeToSecond(time_value)
                    p_return_queue.put(time_value_second)
                else:
                    p_return_queue.put(None)

            # get number of frame of movie to convert
            nb_time_match = re_duration.search(frame_line)
            if nb_time_match and not time_match:
                # create progress bar
                duration_value = nb_time_match.group(0).split(': ')[1]
                total_duration = ffmpegTimeToSecond(duration_value)
                p_return_queue.put(total_duration)


# Class to store convert information
class ConvertInformation:
    def __init__(self, p_video_file, p_list_extract_subtitle_command, p_convert_video_command, p_subtitle_to_remove):
        self.video_file = p_video_file
        self.list_extract_subtitle_command = p_list_extract_subtitle_command
        self.convert_video_command = p_convert_video_command
        self.subtitle_to_remove = p_subtitle_to_remove