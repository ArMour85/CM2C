# Small Python script to convert any video files with subtitle(s) to video supported by Google Chromecast.
#
# Copyright 2021 Arnaud Moura <arnaudmoura@gmail.com>
# This code is released under the terms of the MIT license. See the LICENSE
# file for more details.

# !/usr/bin/env python
# -*- coding: utf-8 -*-


import json
import shlex
import subprocess
import os
import re
from collections import namedtuple
import sys

# Core
from core import capability

# Codec supported
mp4_video_codec_supported = ['h264', 'hevc']
mp4_audio_codec_supported = ['ac3', 'aac', 'mp3', 'eac3']
webm_video_codec_supported = 'vp8'
webm_audio_codec_supported = 'vorbis'


# Define subtitle tuple information
Subtitle = namedtuple('Subtitle', 'codec, track_position, title, language')


# Class to store metadata information
class Metadata:
    class CodecAndLang:
        def __init__(self, p_codec, p_language, p_track_position, p_title=""):
            self.codec = p_codec
            self.language = p_language
            self.title = p_title
            self.track_position = p_track_position

    def __init__(self):
        self.video_codec = ""
        self.video_track_position = ""
        self.video_bit_rate = ""
        self.video_pixelformat = ""
        self.audio_list = []
        self.subtitle_list = []
        self.container = ""
        self.path = ""
        self.name = ""
        self.extension = ""


class MediaSniffer:
    def __init__(self, p_capability):
        MediaSniffer.capability = p_capability

    # function to find the resolution of the input video file
    @staticmethod
    def findVideoMetada(p_path_to_input_video):
        cmd = MediaSniffer.capability.ffprobe_exe + " -v quiet -print_format json -show_streams -show_format"
        args = shlex.split(cmd)
        args.append(p_path_to_input_video)
        # run the ffprobe process, decode stdout & convert to JSON
        try:
            ffprobe_output = subprocess.check_output(args).decode('utf-8')
            return json.loads(ffprobe_output)
        except:
            return None


    # function to get language (country code) of a stream/track
    @staticmethod
    def getLangOfStream(p_stream):
        stream_title = ""
        if 'tags' in p_stream and 'title' in p_stream['tags']:
            stream_title = p_stream['tags']['title']

        country_code = "und"
        if 'tags' in p_stream and 'language' in p_stream['tags']:
            country_code = p_stream['tags']['language']
        else:
            if 'tags' in p_stream:
                country_code = input("I need your help ! What is the country code of this track named '" + stream_title + "' (fr, eng, de, ...) ? ")

        return stream_title, country_code


    # function to test if a file (subtitle) is encoded in UTF-8
    @staticmethod
    def fileIsEncodedInUTF8(p_file):
        data = open(p_file, 'rb').read()
        try:
            return data.decode('utf-8')
        except UnicodeDecodeError:
            return False


    # function to analyse video
    @staticmethod
    def analyseVideo(p_file):
        json_metadata = MediaSniffer.findVideoMetada(p_file)
        # Check output
        if json_metadata == None:
            return None

        metadata_info = Metadata()
        metadata_info.path = p_file
        metadata_info.name = os.path.splitext(p_file)[0]
        metadata_info.extension = os.path.splitext(p_file)[1]
        metadata_info.container = json_metadata['format']['format_name']
        track_counter = 0
        # Get video codec
        for stream in json_metadata['streams']:
            if stream['codec_type'] == 'video' and stream["disposition"]["default"] == 1:
                metadata_info.video_codec = stream['codec_name']
                metadata_info.video_pixelformat = stream['pix_fmt']
                metadata_info.video_track_position = track_counter
                if 'bit_rate' in stream.keys():
                    metadata_info.video_bit_rate = stream['bit_rate']
            elif stream['codec_type'] == 'audio':
                (title, languageValue) = MediaSniffer.getLangOfStream(stream)
                metadata_info.audio_list.append(Metadata.CodecAndLang(stream['codec_name'], languageValue, track_counter, title))
            elif stream['codec_type'] == 'subtitle':
                (title, languageValue) = MediaSniffer.getLangOfStream(stream)
                metadata_info.subtitle_list.append(Metadata.CodecAndLang(stream['codec_name'], languageValue, track_counter, title))

            track_counter += 1
        
        return metadata_info


    # function to check if has good codec and container with one audio track
    @staticmethod
    def hasGoodFormat(metadata_info):
        nb_audio = len(metadata_info.audio_list)
        if (metadata_info.video_codec in mp4_video_codec_supported and "mp4" in metadata_info.container) \
            or (metadata_info.video_codec == webm_video_codec_supported and "webm" in metadata_info.container):
            if nb_audio == 0:
                return False
            elif nb_audio == 1:
                if (metadata_info.audio_list[0].codec in mp4_audio_codec_supported) or (metadata_info.audio_list[0].codec in webm_audio_codec_supported):
                    return False
            else:
                return True
        return True


    # function to analyse subtitles and generate video and subtitle commands
    @staticmethod
    def analyseSubtitle(p_media_info, options):
        has_extracted_srt_subtitle = False
        has_extracted_ass_subtitle = False
        subtitle_path_file_ass = p_media_info.path.replace(p_media_info.extension, ".ass")
        subtitle_path_file_srt = p_media_info.path.replace(p_media_info.extension, ".srt")
        subtitle_command = ""
        subtitle_to_burn = ""
        list_of_extract_subtitle_command = []

        if len(p_media_info.subtitle_list) > 0 and not options.ext_sub and not options.no_sub:
            subtitle_title = ""
            subtitle_codec = ""

            # Contains list of tuple with 3 elements [(subtitleCodec, subtitleTrackPos, subtitleTitle, subtitleLang), ...]
            list_of_subtitle_to_extract = []

            # Analyze subtitles
            for subtitle in p_media_info.subtitle_list:
                if not options.extract_all_sub and subtitle.language in options.sub_language and re.match(options.sub_name_regex, subtitle.language):
                    if subtitle_codec != "":
                        result = "no"
                        if sys.version_info >= (3, 0):
                            result = input("Arg! A subtitle is already find with title '" + subtitle_title + "', do you change it with the subtitle '" + subtitle.title + "' (yes/no) ? ")
                        else:
                            result = raw_input("Arg! A subtitle is already find with title '" + subtitle_title + "', do you change it with the subtitle '" + subtitle.title + "' (yes/no) ? ")

                        if result == "no":
                            continue

                    subtitle_title = subtitle.title
                    subtitle_codec = subtitle.codec
                    list_of_subtitle_to_extract = [Subtitle(subtitle.codec, subtitle.track_position, subtitle.title, subtitle.language)]
                else:
                    list_of_subtitle_to_extract.append(Subtitle(subtitle.codec, subtitle.track_position, subtitle.title, subtitle.language))

            for subtitle in list_of_subtitle_to_extract:
                # Subtitle file path
                if subtitle.codec in ['subrip', 'srt']:
                    # Add lang and title in subtitle name if extract all subtitles
                    if options.extract_all_sub:
                        subtitle_path_file_srt = p_media_info.name + "_" + subtitle.title.replace(" ", "_") + "_" + subtitle.language + ".srt"

                    list_of_extract_subtitle_command.append(MediaSniffer.capability.ffmpeg_exe + ' -y -i "' + p_media_info.path + '" -map 0:' + str(subtitle.track_position) + ' -c copy "' + subtitle_path_file_srt + '"')

                    has_extracted_srt_subtitle = True

                elif subtitle.codec == 'ass':
                    # Add lang in subtitle name if extract all subtitles
                    if options.extract_all_sub:
                        subtitle_path_file_ass = p_media_info.name + "_" + subtitle.title + "_" + subtitle.language + ".ass"

                    list_of_extract_subtitle_command.append(MediaSniffer.capability.ffmpeg_exe + ' -y -i "' + p_media_info.path + '" -map 0:' + str(subtitle.track_position) + ' -c copy "' + subtitle_path_file_ass + '"')

                    has_extracted_ass_subtitle = True

                # Add command to burn the subtitle in the video
                if (has_extracted_srt_subtitle or has_extracted_ass_subtitle) and options.burn_sub and not options.extract_all_sub:
                    if has_extracted_ass_subtitle:
                        subtitle_to_burn = subtitle_path_file_ass
                        subtitle_command = ' -vf ass="' + subtitle_path_file_ass + '"'
                    elif has_extracted_srt_subtitle:
                        subtitle_command = ' -vf subtitles="' + subtitle_path_file_srt  + '"'
                        subtitle_to_burn = subtitle_path_file_srt

        else:
            if options.burn_sub:
                if os.path.exists(subtitle_path_file_srt):
                    if not MediaSniffer.fileIsEncodedInUTF8(subtitle_path_file_srt):
                        print("External subtitle " + subtitle_path_file_srt + " is not encoded in UTF-8. I can't burn it in the video.")
                    else:
                        subtitle_command = ' -vf subtitles="' + subtitle_path_file_srt + '"'
                        subtitle_to_burn = subtitle_path_file_srt
                elif os.path.exists(subtitle_path_file_ass):
                    if not MediaSniffer.fileIsEncodedInUTF8(subtitle_path_file_ass):
                        print("External subtitle " + subtitle_path_file_srt + " is not encoded in UTF-8. I can't burn it in the video.")
                    else:
                        subtitle_command = ' -vf ass="' + subtitle_path_file_ass + '"'
                        subtitle_to_burn = subtitle_path_file_ass

        return subtitle_command, subtitle_to_burn, list_of_extract_subtitle_command


    # function to analyse audio and generate command to convert
    @staticmethod
    def analyseAudio(p_metadata_info, options):
        audio_track_pos = 0
        audio_codec = ""
        audio_command = ""
        for audio in p_metadata_info.audio_list:
            audio_track_pos = audio.track_position
            audio_codec = audio.codec
            if audio.language != "":
                if str.lower(str(audio.language)) == options.audio_language or str.lower(str(audio.language)) == 'und':
                    break

        if audio_codec != "":
            if audio_codec in mp4_audio_codec_supported and not options.force:
                audio_command = ' -c:a copy '
            elif audio_codec == "dts":
                audio_command = ' -c:a ac3 '
            else:
                audio_command = ' -c:a aac '

        return audio_track_pos, audio_command
